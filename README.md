# Parallel computing project
Solve the bin packing problem using parallelization techniques. The algorithm
should consider item's:

* Fragility
* Dimensions
* Weight

## Sample data
These are examples of the data the program will be dealing with. (**NOTE**:
fragility not yet considered)

1. 0.072x0.482x0.304 2.38 Notebook ProBook 640
2. 0.91x0.60x0.265 27.44 Server HPE DL380
3. 0.72x0.34x0.21 6.98 HPE BL460c
4. 0.89x0.57x0.33 23.18 HPE ML150
5. 0.34x0.34x0.095 2.77 HP Switch 2915
6. 0.58x0.84x0.22 14.41 HP Switch 5900AF
7. 0.238x0.757x0.477 8.23 Monitor QHD
8. 0.11x0.19x0.03 0.12 USB-C Travel dock
9. 0.125x0.561x0.387 4.08 Monitor V223
10. 0.155x0.228x0.02 0.08 DDR4 4GB 2400MHz
