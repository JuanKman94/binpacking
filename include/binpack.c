#ifndef BINPACK
#define BINPACK

#include <string.h>
#include <inttypes.h>

#define MAX_NAME_LEN 32

// TODO: make this a linked list
struct item {
	float dim[3];
	float weight;
	char name[MAX_NAME_LEN];
	struct item *next, *prev;
};

struct item new_item()
{
	struct item _i = {
		.weight = 0,
		.next = NULL,
		.prev = NULL
	};

	return _i;
}

int binlen(struct item item)
{
	if (item.next != NULL)
		return 1 + binlen(*item.next);
	else
		return 1;
}

void free_items(struct item *_item)
{
	if (_item != NULL) {
		if (_item->next != NULL)
			free_items(_item->next);

		free(_item);
	}
}

void print_items(struct item _item)
{
	printf("%3.4fx%3.4fx%3.4f %3.4f\t(%s)\n",
				_item.dim[0],
				_item.dim[1],
				_item.dim[2],
				_item.weight,
				_item.name
			);

	if (_item.next != NULL)
		print_items(*_item.next);
}

float *parse_dimensions(char *raw)
{
	float *dimen = NULL;
	int i;
	char *str,
		 *tmp,
		 *saveptr,
		 *endptr;

	dimen = calloc(sizeof(float), 3);
	if (dimen == NULL) return NULL;

	// get width, height and then depth
	for (i = 0, str = raw; i < 3; i++, str = NULL) {
		tmp = strtok_r(str, "x", &saveptr);

		if (tmp == NULL)
			goto err;

		dimen[i] = strtof(tmp, &endptr);
	}

	return dimen;
err:
	if (dimen != NULL) free(dimen);
	return NULL;
}

#endif // BINPACK
