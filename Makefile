# Compiler
CC = gcc

# flags for compiler
# 	-g: debug info to executable object
# 	-Wall: most compiler's warnings enabled
CFLAGS = -g -Wall

# build target
TARGET = binpack

TESTFILE = test.dat

.PHONY: run clean rigger

default: build

build: clean src/main.c
	$(CC) $(CFLAGS) -o $(TARGET) src/main.c

build-rigger: include/rig.c src/rigger.c
	$(CC) $(CFLAGS) -o rigger src/rigger.c

rigger: build-rigger
	./rigger 10x5x5 10 20

run: build
	./$(TARGET) $(TESTFILE)

clean:
	$(RM) $(TARGET)
