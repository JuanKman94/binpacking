#include <stdio.h>
#include <stdlib.h>
#include "../include/binpack.c"

void print_help(char *name)
{
	printf("Usage:\n"
		"\t%s <WxHxD> <w_min> <w_max>\n"
		"Where:\n"
		"\t<WxHxD> are the maximum objects dimensions Width x Height x Depth\n"
		"\t<w_min> is the minimum weight for each item, <w_max> the maximum\n\n"
		"A fragility index is generated for each item\n"
		, name
	);
}

int main(int argc, char *argv[])
{
	float *dimensions;
	int w_min,
		w_max,
		i;
	char *endptr;

	if (argc < 4) {
		print_help(argv[0]);
		return 1;
	}

	dimensions = parse_dimensions(argv[1]);
	if (dimensions == NULL) {
		fprintf(stderr, "error: parse_dimensions\n");
		exit(EXIT_FAILURE);
	}

	w_min = strtol(argv[2], &endptr, 10), endptr = NULL;
	w_max = strtol(argv[3], &endptr, 10), endptr = NULL;

	for (i = 0; i < 3; i++)
		printf("MAIN: dimensions[%d] = %f\n", i, dimensions[i]);

	printf("MAIN: weight max = %d, min = %d\n", w_max, w_min);

	free(dimensions);

	return 0;
}
