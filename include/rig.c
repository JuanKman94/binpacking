#ifndef RIG
#define RIG 1

#include <stdio.h>
#include <stdlib.h>
#include "binpack.c"

#define MAX_NAME 32

int rand_int(int min, int max)
{
	// srand needs to be called previously
	return min + rand() % (max+1 - min);
}

struct item *read_instance(char *fname, float *cap, int *n)
{
	struct item *root = NULL,
				*curr = NULL,
				*prev = NULL;
	float *dim = NULL;
	int i,
		z;	// status
	FILE *f = NULL;
	char *line = NULL,
		 *tmp,
		 *saveptr,
		 *orig;
	size_t len;
	ssize_t read;

	// open file
	f = fopen(fname, "r");
	if (f == NULL) {
		fprintf(stderr, "[ERROR] read_instance: fopen()\n");
		goto err;
	}

	// get fixed bin size & number of items in this file
	z = fscanf(f, "%f %d\n", cap, n); // mind the \n !
	if (z != 2) {
		fprintf(stderr, "[ERROR] read_instance: fscanf(), return %d\n", z);
		goto err;
	}

	// parse each item
	for (i = 0; i < *n; i++) {
		// init pointers for getline
		if (line != NULL) {
			free(line);
			line = NULL;
		}

		if ( (read = getline(&line, &len, f)) != -1 ) {
			// we need to free the memory allocated by getline(),
			// but strtok() messes the pointer, so we make a copy
			orig = line;

			// allocate memory for new item
			curr = malloc(sizeof(struct item));
			if (curr == NULL) {
				fprintf(stderr, "[ERROR] read_instance: malloc()\n");
				break;
			}

			if (root == NULL) {
				root = curr;
			}
			if (prev != NULL) {
				curr->prev = prev;
				prev->next = curr;
			}

			// fill item struct from line
			{
				// parse dimensions
				tmp = strtok(line, " ");
				dim = parse_dimensions(tmp);
				if (dim == NULL) {
					fprintf(stderr, "[ERROR] read_instance: parse_dimensions()\n");
					goto err;
				}
				// copy the values
				curr->dim[0] = dim[0];
				curr->dim[1] = dim[1];
				curr->dim[2] = dim[2];

				// parse weight
				tmp = strtok(NULL, " ");
				curr->weight = strtof(tmp, &saveptr);

				// copy its name
				tmp = strtok(NULL, "\n");
				strncpy(curr->name, tmp, MAX_NAME_LEN);
			}

			prev = curr;
			line = orig;
		} else {
			fprintf(stderr, "[ERROR] read_instance: getline()\n");
			goto err;
		}
	}

	fclose(f);
	return root;
err:
	if (f != NULL) fclose(f);
	if (root != NULL) free(root);
	if (line != NULL) free(line);

	return NULL;
}

#endif // RIG
