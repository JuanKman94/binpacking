#include <stdio.h>
#include <stdlib.h>

#include "../include/rig.c"
#include "../include/binpack.c"

int main(int argc, char *argv[])
{
	struct item *item = NULL;
	float cap = 0.0;
	int n = 0;

	item = read_instance(argv[1], &cap, &n);
	printf("cap = %f;\tn = %d\n", cap, n);
	if (item != NULL) {
		print_items(*item);
		printf("Total items: %d\n", binlen(*item));

		free_items(item);
	}

	return 0;
}
